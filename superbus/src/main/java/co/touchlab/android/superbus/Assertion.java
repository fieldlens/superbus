package co.touchlab.android.superbus;

final class Assertion
{
    static void nonNull(final Object... objects)
    {
        for (int i = 0; i < objects.length; ++i)
        {
            if (objects[i] == null)
            {
                throw new NullPointerException("Object " + i + " is null!");
            }
        }
    }

    private Assertion() {}
}
