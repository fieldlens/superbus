package co.touchlab.android.superbus;

import android.content.Context;

import java.io.Serializable;

import co.touchlab.android.superbus.errorcontrol.PermanentException;
import co.touchlab.android.superbus.errorcontrol.TransientException;

/**
 * Abstract class to wrap the operation you want to run.
 *
 * Commands are added to the PersistenceProvider, and called by the SuperbusService.
 * Commands will be processed in the order that they are added, unless a priority is set.
 * Commands with the same priority will be processed in the order they are added.
 *
 * Higher priority commands are processed BEFORE lower priority.  For example, priority 3 commands are processed after
 * priority 7 commands.
 *
 * Default priority is 10.
 *
 * Commands must include a no-arg constructor.  The bus service will fail without this.
 *
 * If your commands should be persisted, use StoredCommand instead, or some other custom
 * implementation.
 *
 * User: kgalligan
 * Date: 1/11/12
 * Time: 8:57 AM
 */
public interface Command extends Serializable
{
    int MUCH_LOWER_PRIORITY = 1;
    int LOWER_PRIORITY = 5;
    int DEFAULT_PRIORITY = 10;
    int HIGHER_PRIORITY = 15;
    int MUCH_HIGHER_PRIORITY = 20;

    /**
     * This is for your benefit.  Command info will be logged during various events.
     * @return String representation of the command.  Human readable.  Bus doesn't care what this is, but keep in mind common sense performance considerations.
     */
    String logSummary();

    /**
     * This is like Java equals, but could be more relaxed.  Used to test existing commands and exclude adding a new one.
     * The idea is if you have a generic command that only needs to run once, adding multiples will be excluded.  For example
     * say you have a "RefreshAccounts" command.  Running that multiple times would probably be wasteful.
     * To ALWAYS run the command, simply return false.
     *
     * @param command Command to test for "same-ness"
     * @return true if passed command is the same as this one.  False if not, or if the command should always run.
     */
    @Deprecated // will be removed in near future, when comparison moved to db
    boolean same(Command command);

    /**
     * This is where your logic goes.  You must be very careful with how you handle error conditions.
     *
     * "Soft" issues should throw a TransientException.  This is almost always due to temporary network issues.  These
     * will cause the command to be put back on the queue, and processing to stop.  This preserves the command so its not
     * dropped.  HOWEVER!!!  If you throw a TransientException for something that won't resolve itself, by default
     * it will NEVER be removed.  You will build up commands forever, and (worse) stop further processing.
     *
     * To eventually quit command processing attempts, use a custom implementation of CommandPurgePolicy.
     *
     * Also, if processing is stopped due to a TransientException, that command and possibly others will remain
     * in the queue.  Some other event will be required to restart processing.  Either manual, or a network connection
     * listener.
     *
     * "Hard" issues should throw a PermanentException.  This will remove the command, and call onPermanentError.  Your
     * code will need to deal with the error.  Possibly show a notification to the user, revert DB changes, etc.  Processing
     * will continue on other commands, if they exist.
     *
     * Be aware, if latter commands depend on this one, they will probably trigger a cascade of errors.  Just FYI. Plan
     * accordingly.
     *
     * Any unchecked exception coming out of this call will be interpreted as a permanent issue, and also removed. This
     * includes instances of RuntimeException and Error.  In future releases, there may be a need to handle OutOfMemoryError
     * and similar issues in a different way.  If the command always triggers memory limits, it should be removed, but
     * if memory issues are triggered because the rest of the app was using too much memory temporarily, the command may
     * be removed unnecessarily.  Future releases should probably allow the SuperbusService to run in a separate process
     * to reduce the likelihood of this, but that is not possible today.
     *
     * @param context
     * @throws co.touchlab.android.superbus.errorcontrol.TransientException Something happened that will resolve itself.
     * @throws co.touchlab.android.superbus.errorcontrol.PermanentException Something happened that we can't recover from.
     */
    void callCommand(Context context) throws TransientException, PermanentException;

    /**
     * There was a transient problem with this command.  Its being put back on the queue.
     *
     * @param exception Exception that caused the removal
     */
    void onTransientError(Context context, TransientException exception);

    /**
     * There was a permanent problem with this command and its being removed.  You will probably want to put some type of
     * notification or reversal code in this method.
     *
     * @param exception Exception that caused the removal
     */
    void onPermanentError(Context context, PermanentException exception);

    /**
     * Success!  Your command processed.
     */
    void onSuccess(Context context);

}
