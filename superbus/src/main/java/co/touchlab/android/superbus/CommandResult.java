package co.touchlab.android.superbus;

/**
 * Extracting enum from SuperbusProcessor
 * <br>
 * User: Andrew
 * Date: 4/4/17
 * Time: 10:36 AM
 */
public enum CommandResult
{
    SUCCESS, TRANSIENT, PERMANENT
}
