package co.touchlab.android.superbus;

public interface ProcessingInterrupter
{
    boolean isInterrupted();
}
