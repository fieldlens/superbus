package co.touchlab.android.superbus;

import android.content.Context;
import android.support.annotation.NonNull;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import co.touchlab.android.superbus.log.BusLog;
import co.touchlab.android.superbus.utils.UiThreadContext;

/**
 * The heart of the command bus.  Processes commands.
 * <p/>
 * User: kgalligan
 * Date: 1/11/12
 * Time: 8:57 AM
 */
public final class Superbus
{
    private static final String TAG = "Superbus";
    private static final Lock lock = new ReentrantLock();
    private static SuperbusConfig config;

    public static void init(@NonNull final SuperbusConfig config)
    {
        Assertion.nonNull(config);
        Superbus.config = config;
    }

    public static void runProcessingLoop(@NonNull final Context context,
                                         @NonNull final ProcessingInterrupter interrupter)
    {
        Assertion.nonNull(context);
        UiThreadContext.assertBackgroundThread();

        if (lock.tryLock())
        {
            final BusLog logger = getConfig().getLog();
            logger.d(TAG, "start queue processing");

            try
            {
                new SuperbusProcessor(context, getConfig(), interrupter).processCommandsQueue();
            }
            finally
            {
                logger.d(TAG, "finish queue processing");
                lock.unlock();
            }
        }
    }

    public static SuperbusConfig getConfig()
    {
        assertInited();
        return config;
    }

    private static void assertInited()
    {
        if (config == null) throw new IllegalStateException("Call init() first");
    }

    private Superbus() {}
}
