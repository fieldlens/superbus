package co.touchlab.android.superbus;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import co.touchlab.android.superbus.errorcontrol.CommandPurgePolicy;
import co.touchlab.android.superbus.errorcontrol.TransientMethuselahCommandPurgePolicy;
import co.touchlab.android.superbus.log.BusLog;
import co.touchlab.android.superbus.log.BusLogImpl;
import co.touchlab.android.superbus.storage.PersistenceProvider;

/**
 * Created with IntelliJ IDEA.
 * User: kgalligan
 * Date: 12/25/13
 * Time: 12:36 AM
 * To change this template use File | Settings | File Templates.
 */
public final class SuperbusConfig
{
    @NonNull private final List<SuperbusEventListener> eventListeners = new ArrayList<>();
    @NonNull private final BusLog log;
    @NonNull private final CommandPurgePolicy commandPurgePolicy;
    @NonNull private final PersistenceProvider persistenceProvider;

    private SuperbusConfig(@NonNull final List<SuperbusEventListener> eventListeners,
                           @NonNull final BusLog log,
                           @NonNull final CommandPurgePolicy commandPurgePolicy,
                           @NonNull final PersistenceProvider persistenceProvider)
    {
        Assertion.nonNull(eventListeners, log, commandPurgePolicy, persistenceProvider);

        this.eventListeners.addAll(eventListeners);
        this.log = log;
        this.commandPurgePolicy = commandPurgePolicy;
        this.persistenceProvider = persistenceProvider;
    }

    @NonNull public List<SuperbusEventListener> getEventListeners()
    {
        return Collections.unmodifiableList(eventListeners);
    }

    @NonNull public BusLog getLog()
    {
        return log;
    }

    @NonNull public CommandPurgePolicy getCommandPurgePolicy()
    {
        return commandPurgePolicy;
    }

    @NonNull public PersistenceProvider getPersistenceProvider()
    {
        return persistenceProvider;
    }

    public static class Builder
    {
        private List<SuperbusEventListener> eventListeners = new ArrayList<>();
        private BusLog busLog = new BusLogImpl();
        private CommandPurgePolicy commandPurgePolicy = new TransientMethuselahCommandPurgePolicy();
        private PersistenceProvider persistenceProvider;

        public Builder addEventListener(@NonNull final SuperbusEventListener eventListener)
        {
            Assertion.nonNull(eventListener);
            eventListeners.add(eventListener);
            return this;
        }

        public Builder setLog(@NonNull final BusLog log)
        {
            Assertion.nonNull(log);
            busLog = log;
            return this;
        }

        public Builder setCommandPurgePolicy(@NonNull final CommandPurgePolicy purgePolicy)
        {
            Assertion.nonNull(purgePolicy);
            commandPurgePolicy = purgePolicy;
            return this;
        }

        public Builder setPersistenceProvider(@NonNull final PersistenceProvider provider)
        {
            Assertion.nonNull(provider);
            persistenceProvider = provider;
            return this;
        }

        public SuperbusConfig build()
        {
            Assertion.nonNull(persistenceProvider);
            return new SuperbusConfig(eventListeners, busLog, commandPurgePolicy, persistenceProvider);
        }
    }
}
