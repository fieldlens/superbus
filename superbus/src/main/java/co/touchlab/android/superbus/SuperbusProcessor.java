package co.touchlab.android.superbus;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;

import java.util.List;

import co.touchlab.android.superbus.errorcontrol.CommandPurgePolicy;
import co.touchlab.android.superbus.errorcontrol.PermanentException;
import co.touchlab.android.superbus.errorcontrol.StorageException;
import co.touchlab.android.superbus.errorcontrol.SuperbusProcessException;
import co.touchlab.android.superbus.errorcontrol.TransientException;
import co.touchlab.android.superbus.log.BusLog;
import co.touchlab.android.superbus.storage.PersistenceProvider;

/**
 * Created with IntelliJ IDEA.
 * User: kgalligan
 * Date: 4/3/13
 * Time: 1:38 PM
 * To change this template use File | Settings | File Templates.
 */
final class SuperbusProcessor
{
    private static final String TAG = SuperbusProcessor.class.getSimpleName();

    private @NonNull final List<SuperbusEventListener> eventListeners;
    private @NonNull final CommandPurgePolicy purgePolicy;
    private @NonNull final PersistenceProvider provider;
    private @NonNull final BusLog log;
    private @NonNull final Handler mainThreadHandler;
    private @NonNull final Context appContext;
    private @NonNull final ProcessingInterrupter processingInterrupter;

    SuperbusProcessor(@NonNull final Context context,
                      @NonNull final SuperbusConfig config,
                      @NonNull final ProcessingInterrupter interrupter)
    {
        Assertion.nonNull(context, config, interrupter);

        appContext = context;
        eventListeners = config.getEventListeners();
        purgePolicy = config.getCommandPurgePolicy();
        log = config.getLog();
        provider = config.getPersistenceProvider();
        processingInterrupter = interrupter;
        mainThreadHandler = new Handler(Looper.getMainLooper());
    }

    private void logCommandDebug(Command command, String methodName)
    {
        try
        {
            log.d(TAG, methodName + ": " + command.logSummary());
        }
        catch (Exception e)
        {
            //Just in case...
        }
    }

    private void logCommandVerbose(Command command, String methodName)
    {
        try
        {
            log.v(TAG, methodName + ": " + command.logSummary());
        }
        catch (Exception e)
        {
            //Just in case...
        }
    }

    public void processCommandsQueue()
    {
        log.i(TAG, "CommandThread loop started");

        Command command;

        try
        {
            for (SuperbusEventListener eventListener : eventListeners)
            {
                eventListener.onBusStarted(appContext, provider);
            }

            provider.logPersistenceState();

            while (provider.hasTop() && !processingInterrupter.isInterrupted())
            {
                command = provider.readTop();
                CommandResult commandResult;
                Throwable cause;

                logCommandDebug(command, "[CommandThread]");

                for (SuperbusEventListener eventListener : eventListeners)
                {
                    eventListener.onCommandStarted(appContext, command);
                }

                try
                {
                    callCommand(command);
                    cause = null;
                    commandResult = CommandResult.SUCCESS;
                }
                catch (TransientException e)
                {
                    cause = e;

                    boolean purge = purgePolicy.purgeCommandOnTransientException(command, e);

                    if (purge)
                    {
                        log.w(TAG, "Purging command on TransientException: {" + command.logSummary() + "}");
                        commandResult = CommandResult.PERMANENT;
                    }
                    else
                    {
                        commandResult = CommandResult.TRANSIENT;
                    }
                }
                catch (Throwable e)
                {
                    cause = e;
                    commandResult = CommandResult.PERMANENT;
                }

                if (cause != null)
                    log.e(TAG, null, cause);

                //Deal with status
                switch (commandResult)
                {
                    case SUCCESS:
                        provider.removeCommand(command);
                        command.onSuccess(appContext);
                        break;

                    case TRANSIENT:
                        logTransientException(command, cause);
                        break;

                    case PERMANENT:
                        provider.removeCommand(command);
                        logPermanentException(command, cause);
                        break;

                    default:
                        throw new SuperbusProcessException("Unknown status");
                }

                log.d(TAG, "Command [" + command.getClass().getSimpleName() + "] ended: " + System.currentTimeMillis());

                for (SuperbusEventListener eventListener : eventListeners)
                {
                    eventListener.onCommandFinished(appContext, command, commandResult);
                }

                //Must leave loop for a bit
                if (commandResult == CommandResult.TRANSIENT)
                    break;
            }
        }
        catch (StorageException e)
        {
            throw new SuperbusProcessException(e);
        }

        mainThreadHandler.post(new Runnable()
        {
            @Override
            public void run()
            {
                onFinishProcessing(appContext);
            }
        });
    }

    private void logTransientException(Command c, Throwable e)
    {
        log.e(TAG, null, e);
        TransientException pe = e instanceof TransientException ? (TransientException) e : new TransientException(e);
        c.onTransientError(appContext, pe);
    }

    private void logPermanentException(Command c, Throwable e)
    {
        log.e(TAG, null, e);
        PermanentException pe = e instanceof PermanentException ? (PermanentException) e : new PermanentException(e);
        c.onPermanentError(appContext, pe);
    }

    private void onFinishProcessing(Context context)
    {
        try
        {
            for (SuperbusEventListener eventListener : eventListeners)
            {
                eventListener.onBusFinished(context, provider, provider.getSize() == 0);
            }
        }
        catch (StorageException e)
        {
            log.e(TAG, null, e);
        }
    }

    private void callCommand(final Command command) throws Exception
    {
        logCommandVerbose(command, "callCommand-start");

        command.callCommand(appContext);

        logCommandVerbose(command, "callComand-finish");
    }
}
